dynlib.metio.nora10
===================

.. automodule:: dynlib.metio.nora10
   :members:
   :undoc-members:

   Public functions
   ---------

   .. autofunction:: get_instantaneous
   .. autofunction:: get_time_average
   .. autofunction:: get_aggregate
   .. autofunction:: get_composite
   .. autofunction:: metopen
   .. autofunction:: metsave
   .. autofunction:: metsave_composite

   Internal functions
   ------------------


